const auth = require('./auth');
const events = require('./events');

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

/**
 * Lets be nice to their server by sleeping for a few seconds between each request during this demo script.
 */
const sleep = (milliseconds) => new Promise((resolve) => {
    setTimeout(() => resolve(), milliseconds);
});

const main = async () => {

    if (process.argv.length < 3) {
        console.err("Usage:\n\n  nodejs main.js username password\n\n  nodejs main.js sessionId");
        return;
    }

    let sessionId = null;
    if (process.argv.length === 3) {
        sessionId = process.argv[2];
        console.debug("Using existing session");
    } else {
        const username = process.argv[2];
        const password = process.argv[3];
        console.debug("Obtaining new session for", username);

        sessionId = await auth.login(username, password);
        console.debug("Obtained new session", sessionId);
    }

    try {
        // const eoiPage = await events.fetchEoiDetails(sessionId, '0079718');
        // console.log(eoiPage);

        // const eoiResponse = await events.submitEoi(sessionId, eoiPage.pdn, eoiPage.shifts[0].value);
        // console.log(eoiResponse);

        // const eventsForDivision = await events.fetchEvents(sessionId);
        // console.log("Local events", eventsForDivision);
        // await sleep(2000);

        // const eventsForState = await events.fetchEventsForDivision(sessionId, 3);
        // console.log("State events", eventsForState);
        // await sleep(2000);

        const eventsFromMyRoster = await events.fetchEventsFromMyRoster(sessionId);
        console.log("My roster", eventsFromMyRoster);
        // await sleep(2000);

        // const myPastEvents = await events.fetchMyHours(sessionId);
        // console.log("My past events", myPastEvents);
        // await sleep(2000);
    } catch (error) {
        if (error.name === "SessionExpiredError" && process.argv.length === 3) {
            console.error("Using an expired session ID. Please log in again with a username and password.");
        }

        throw error;
    }
};

main();