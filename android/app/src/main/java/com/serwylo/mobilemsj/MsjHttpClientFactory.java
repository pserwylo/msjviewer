package com.serwylo.mobilemsj;

import android.os.Build;
import android.util.Log;

import com.facebook.react.modules.network.OkHttpClientFactory;
import com.facebook.react.modules.network.OkHttpClientProvider;
import com.facebook.react.modules.network.ReactCookieJarContainer;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Nullable;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.Call;
import okhttp3.CipherSuite;
import okhttp3.Connection;
import okhttp3.ConnectionSpec;
import okhttp3.Cookie;
import okhttp3.CookieJar;
import okhttp3.EventListener;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Protocol;
import okhttp3.TlsVersion;

public class MsjHttpClientFactory implements OkHttpClientFactory {

    @Override
    public OkHttpClient createNewNetworkModuleClient() {
        try {
            Log.d("ReactNative", "createNewNetworkModuleClient");
            OkHttpClient.Builder client = OkHttpClientProvider.createClientBuilder()
                    // .socketFactory(createSslSocketFactory())
                    // .eventListener(createLoggingEventListener())
                    .cookieJar(createEmptyCookieJar())
                    // .hostnameVerifier(createHostnameVerifier())
                    .connectionSpecs(createHackyMsjSpecificConnectionSpecs());
            ;

            return OkHttpClientProvider.enableTls12OnPreLollipop(client).build();

        } catch (Exception e) {
            Log.d("ReactNative", "Error in createNewNetworkModuleClient: " + e.getMessage());
            throw new RuntimeException(e);
        }
    }

    private List<ConnectionSpec> createHackyMsjSpecificConnectionSpecs() {

        List<ConnectionSpec> specs = new ArrayList<>();

        // We would like to just use MODERN_TLS, but this is busted on not -to-old versions of Android
        // specs.add(ConnectionSpec.MODERN_TLS);

        // Yes, certain versions of Android are buggy and the MSJ website has
        // a terrible SSL configuration, combining in the perfect storm where
        // we don't use SSL on older versions of Android.
        specs.add(ConnectionSpec.CLEARTEXT);

        // Based on https://www.ssllabs.com/ssltest/analyze.html?d=ssl.stjohnvic.com.au.
        specs.add(
                new ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
                        .tlsVersions(TlsVersion.TLS_1_2)
                        .supportsTlsExtensions(true)
                        .cipherSuites(
                                CipherSuite.TLS_DHE_RSA_WITH_AES_128_CBC_SHA,
                                CipherSuite.TLS_DHE_RSA_WITH_3DES_EDE_CBC_SHA,
                                CipherSuite.TLS_ECDHE_RSA_WITH_3DES_EDE_CBC_SHA,
                                CipherSuite.TLS_DHE_RSA_WITH_AES_128_CBC_SHA,
                                CipherSuite.TLS_DHE_RSA_WITH_CAMELLIA_128_CBC_SHA,
                                CipherSuite.TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA, // Android 7.0 (3rd)
                                CipherSuite.TLS_DHE_RSA_WITH_AES_128_CBC_SHA256,
                                CipherSuite.TLS_DHE_RSA_WITH_AES_128_GCM_SHA256,
                                CipherSuite.TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA256,
                                CipherSuite.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256, // Android 7.0 (1st)
                                CipherSuite.TLS_DHE_RSA_WITH_AES_256_CBC_SHA,
                                CipherSuite.TLS_DHE_RSA_WITH_CAMELLIA_256_CBC_SHA,
                                CipherSuite.TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA, // Android 7.0 (4th)
                                CipherSuite.TLS_DHE_RSA_WITH_AES_256_CBC_SHA256,
                                CipherSuite.TLS_DHE_RSA_WITH_AES_256_GCM_SHA384,
                                CipherSuite.TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA384,
                                CipherSuite.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384 // Android 7.0 (2nd)
                        )
                        .build()
        );

        // See OkHttpClientProvider#enableTls12OnPreLollipop().
        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN && Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
            specs.add(
                    new ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
                            .tlsVersions(TlsVersion.TLS_1_2)
                            .build()
            );

            specs.add(ConnectionSpec.COMPATIBLE_TLS);
        } else {
            specs.add(
                    new ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
                            .tlsVersions(TlsVersion.TLS_1_2)
                            .cipherSuites(
                                    CipherSuite.TLS_DHE_RSA_WITH_AES_256_GCM_SHA384,
                                    CipherSuite.TLS_DHE_RSA_WITH_AES_128_GCM_SHA256,
                                    CipherSuite.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384
                            )
                            .build()
            );
        }*/

        return specs;
    }

    private static EventListener createLoggingEventListener() {
        return new EventListener() {
            @Override
            public void connectFailed(Call call, InetSocketAddress inetSocketAddress, Proxy proxy, @Nullable Protocol protocol, IOException ioe) {
                Log.d("ReactNative", "connectFailed");
            }

            @Override
            public void connectionAcquired(Call call, Connection connection) {
                Log.d("ReactNative", "connectionAcquired");
            }

            @Override
            public void callFailed(Call call, IOException ioe) {
                Log.d("ReactNative", "callFailed");
            }
        };
    }

    private static CookieJar createEmptyCookieJar() {
        return new ReactCookieJarContainer() {
            @Override
            public void saveFromResponse(HttpUrl url, List<Cookie> cookies) {
                // Don't persist cookies automatically, let the client pass them whenever required.
            }
        };
    }

    private static HostnameVerifier createHostnameVerifier() {
        return new HostnameVerifier() {
            @Override
            public boolean verify(String hostname, SSLSession session) {
                Log.d("ReactNative", "verifying host name");
                return true;
            }
        };
    }

    private static SSLSocketFactory createSslSocketFactory() {
        Log.d("ReactNative", "createSslSocketFactory");
        try {
            SSLContext sslContext = SSLContext.getInstance("TLSv1");
            sslContext.init(null, createOverlyTrustingTrustManager(), new SecureRandom());
            return sslContext.getSocketFactory();
        } catch (KeyManagementException | NoSuchAlgorithmException e) {
            Log.d("ReactNative", "Error in createSslSocketFactory: " + e.getMessage());
            throw new RuntimeException(e);
        }
    }

    // Create a trust manager that does not validate certificate chains
    private static TrustManager[] createOverlyTrustingTrustManager() {
        return new TrustManager[] {
                new X509TrustManager() {
                    @Override
                    public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        Log.d("ReactNative", "checkClientTrusted");
                    }

                    @Override
                    public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                        Log.d("ReactNative", "checkServerTrusted");
                    }

                    @Override
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        Log.d("ReactNative", "getAcceptedIssuers");
                        return null;
                    }
                }
        };
    }
}
