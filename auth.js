import * as axios from 'axios';
import querystring from 'query-string';

import {BASE_URL, AuthError} from "./net";

const sessionIdFromHeaders = (headers) => {

    console.log("Looking for session ID in HTTP response");

    if (!headers.hasOwnProperty('set-cookie') || !(headers['set-cookie'] instanceof Array) || headers['set-cookie'].length === 0) {
        console.log("No set-cookie header found in: " + JSON.stringify(headers));
        return null;
    }

    console.log("Found set-cookie header(s): " + JSON.stringify(headers['set-cookie']));

    const match = headers['set-cookie'][0].match(/.*JSESSIONID=(\w*);.*/);
    return match.length > 1 ? match[1] : null;
};

const fetchNewSessionIdFromServer = async () => {
    const data = await axios.get(`${BASE_URL}/`);
    return sessionIdFromHeaders(data.headers);
};

export const login = async (username, password) => {

    const initialSessionId = await fetchNewSessionIdFromServer();

    const data = {
        j_username: username,
        j_password: password,
    };

    const response = await axios.post(`${BASE_URL}/j_security_check`, querystring.stringify(data), {
        maxRedirects: 1, // We expect to be redirected to a new page, and receive a new session cookie in response.
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Cookie': `JSESSIONID=${initialSessionId}`,
        },
    });

    const sessionId = sessionIdFromHeaders(response.headers);
    if (sessionId == null) {
        throw new AuthError("Couldn't obtain session ID when logging in");
    }

    return sessionId;
};
