import {AsyncStorage} from "react-native";
import {login} from "./auth";
import {fetchEventsForDivision, fetchEventsFromMyRoster, fetchMyHours, fetchRegions} from "./events";

const defaultState = {
    auth: {
        username: null,
        password: null,
        sessionId: null,
    },
    splashScreen: {
        isLoading: false,
        error: null,
    },
    eventsData: {
        eventsByDivision: {},
        myUpcomingEvents: [],
        myPastEvents: [],
        divisions: [],
    },
};

const LOGIN_SUCCESSFUL = 'LOGIN_SUCCESSFUL';
const LOGIN_START = 'LOGIN_START';
const LOGIN_FAILED = 'LOGIN_FAILED';

const loginStart = (username) => ({type: LOGIN_START, username});
const loginFailed = () => ({type: LOGIN_FAILED});
const loginSuccessful = (username, password, sessionId) => ({type: LOGIN_SUCCESSFUL, username, password, sessionId});

const DATA_RECEIVED = 'DATA_RECEIVED';

const dataReceived = (divisions, eventsByDivision, myUpcomingEvents, myPastEvents) => ({
    type: DATA_RECEIVED,
    divisions,
    eventsByDivision,
    myUpcomingEvents,
    myPastEvents
});

export const actionCreators = {

    onAttemptLogin: (username, password) => {
        return async dispatch => {
            try {
                dispatch(loginStart(username));
                const sessionId = await login(username, password);
                await dispatch(actionCreators.fetchData(sessionId));
                dispatch(loginSuccessful(username, password, sessionId));
            } catch (error) {
                console.log("Error logging in from splash screen.", error);
                dispatch(loginFailed());
            }
        }
    },

    fetchData: (sessionId) => {

        return async dispatch => {
            const divisions = await fetchRegions(sessionId);

            const eventsByDivision = {};
            for (let i = 0; i < divisions.length; i++) {
                const division = divisions[i];
                const eventsData = await fetchEventsForDivision(sessionId, division.id);
                eventsByDivision[division.id] = eventsData.events;
            }

            const myUpcomingEventsData = await fetchEventsFromMyRoster(sessionId);
            const myUpcomingEvents = myUpcomingEventsData.events;

            const myPastEventsData = await fetchMyHours(sessionId);
            const myPastEvents = myPastEventsData.events;

            dispatch(
                dataReceived(
                    divisions,
                    eventsByDivision,
                    myUpcomingEvents,
                    myPastEvents,
                )
            );
        }
    }

};

export const reducer = (state = defaultState, action) => {
    switch(action.type) {

        case DATA_RECEIVED:
            return {
                ...state,
                eventsData: {
                    divisions: action.divisions,
                    eventsByDivision: action.eventsByDivision,
                    myUpcomingEvents: action.myUpcomingEvents,
                    myPastEvents: action.myPastEvents
                }
            };

        /**
         * Don't store the password yet, wait until we know we were able to successfully log in before doing that.
         */
        case LOGIN_START:
            return {
                ...state,
                auth: {
                    ...state.auth,
                    username: action.username,
                },
                splashScreen: {
                    isLoading: true,
                    error: null,
                }
            };

        case LOGIN_FAILED:
            return {
                ...state,
                splashScreen: {
                    isLoading: false,
                    error: 'Could not log you into MSJ. Please check your username, password, and internet connection.',
                }
            };

        /**
         * We already have the username from when we started the login process. Therefore, we only need to store
         * the password (now we know it was successful) and the session ID we received.
         */
        case LOGIN_SUCCESSFUL:
            return {
                ...state,
                splashScreen: {
                    isLoading: false,
                    error: null,
                },
                auth: {
                    ...state.auth,
                    password: action.password,
                    sessionId: action.sessionId
                }
            };
    }
    return state;
};