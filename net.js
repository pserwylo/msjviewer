import * as axios from 'axios';
import cheerio from 'cheerio';
import querystring from 'query-string';

export const BASE_URL = 'http://ssl.stjohnvic.com.au/msj';

const getMsjPage = async (url, sessionId) => {
    console.log(`GET ${url}`);
    return axios.get(url, {
        headers: {
            'Cookie': `JSESSIONID=${sessionId}`,
        }
    });
};

const postMsjPage = async (url, sessionId, data) => {
    console.log(`POST ${url}`);
    return await axios.post(url, querystring.stringify(data), {
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Cookie': `JSESSIONID=${sessionId}`,
        }
    });
};

const responseToDom = async (response) => {
    const body = await response.data;
    const $ = cheerio.load(body);

    const usernameInput = $('input[name=j_username]');

    if (usernameInput.length > 0) {
        console.log("Authentication error while fetching content from server. Found a login form in the response, which implies our session ID is no good.");
        throw new AuthError();
    }

    return $('body');
};

export const get = async (url, sessionId) => {
    const response = await getMsjPage(url, sessionId);
    return responseToDom(response);
};

export const post = async (url, sessionId, data) => {
    const response = await postMsjPage(url, sessionId, data);
    return responseToDom(response);
};

export class AuthError extends Error {
    constructor() {
        super("Authentication failed");
        this.name = "AuthError";
    }
}