import {Icon} from "native-base";
import AndroidBackButton from 'react-native-android-back-btn';

import React from "react";

export const BackIcon = ({onBack = null}) =>
    <Icon ios='ios-arrow-back' android="md-arrow-back" name="arrow-back">
        {onBack ? <AndroidBackButton onPress={() => { onBack(); return true }} /> : null}
    </Icon>;