import React from 'react';
import { createStackNavigator } from 'react-navigation';
import { MainScreen } from './screens/MainScreen';
import { SplashScreen } from './screens/SplashScreen';
import { EventsListScreen } from './screens/EventsListScreen';
import { EventScreen } from './screens/EventScreen';
import * as colours from "./components/colours";

import { Provider, connect } from 'react-redux'
import { createStore, applyMiddleware, combineReducers, compose} from 'redux'
import thunkMiddleware from 'redux-thunk'
import {createLogger} from 'redux-logger'
import {reducer, actionCreators} from './reducers'
import { persistStore, persistReducer } from 'redux-persist'
import { PersistGate } from 'redux-persist/integration/react'
import storage from 'redux-persist/lib/storage'

const loggerMiddleware = createLogger({ predicate: (getState, action) => __DEV__  });

function configureStore() {
    const enhancer = compose(
        applyMiddleware(
            thunkMiddleware, // lets us dispatch() functions
            loggerMiddleware,
        ),
    );

    const persistorConfig = {
        key: 'root',
        storage,
        whitelist: ['auth', 'eventsData'],
    };

    const persistedStore = persistReducer(persistorConfig, reducer);

    return createStore(persistedStore, enhancer);
}

const store = configureStore();
const persistor = persistStore(store);

const connectedMainScreen = () => {
    const mapStateToProps = state => (state.eventsData);

    const mapDispatchToProps = dispatch => {
        return {
            onSettings: () => {},
            onRefresh: () => {},
            onViewEventList: () => {},
            onViewEvent: () => {},
        }
    };

    return connect(mapStateToProps, mapDispatchToProps)(MainScreen);
};

const connectedSplashScreen = () => {
    const mapStateToProps = state => ({
        ...state.auth,
        ...state.splashScreen,
    });

    const mapDispatchToProps = dispatch => {
        return {
            onAttemptLogin: (username, password) => dispatch(actionCreators.onAttemptLogin(username, password)),
        }
    };

    return connect(mapStateToProps, mapDispatchToProps)(SplashScreen);
};

const connectedEventsListScreen = () => {
    const mapStateToProps = state => ({
        ...state.eventsData,
    });

    const mapDispatchToProps = dispatch => {
        return {
            onViewEvent: () => {},
            onRefresh: () => {},
        }
    };

    return connect(mapStateToProps, mapDispatchToProps)(EventsListScreen);
};

const connectedEventScreen = () => {
    const mapStateToProps = state => ({
        sessionId: state.auth.sessionId,
    });

    const mapDispatchToProps = dispatch => {
        return {
            onEoi: () => {},
        }
    };

    return connect(mapStateToProps, mapDispatchToProps)(EventScreen);
};

const RootStack = createStackNavigator(
    {
        Main: { screen: connectedMainScreen() },
        Splash: { screen: connectedSplashScreen() },
        EventsList: { screen: connectedEventsListScreen() },
        Event: { screen: connectedEventScreen() },
        /* AuthScreen: { screen: AuthScreen },
        LoadingScreen: { screen: LoadingScreen },*/
    },
    {
        initialRouteName: 'Splash',
        navigationOptions: {
            title: 'Mobile MSJ',
            headerStyle: {
                backgroundColor: colours.theme,
            },
            headerTintColor: 'white',
        }
    }
);

export const App = () => (
    <Provider store={store}>
        <PersistGate persistor={persistor}>
            <RootStack />
        </PersistGate>
    </Provider>
);
