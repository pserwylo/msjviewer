import {FORM_SCHEMA, Schemas} from './schemas';

export const getForms = async () => {
    const realm = await Realm.open({schema: Schemas});
    return realm.objects('Form');
};

export const addForm = async (name) => {
    const realm = await Realm.open({schema: Schemas});
    realm.write(() => {
        realm.create('Form', {id: 12, name: name});
    })
};
