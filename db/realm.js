import * as schemas from './schemas';

import Realm from 'realm';

/**
 * Return a promise of a realm obuect, so that we can talk to it when the promise resolves.
 * @returns {ProgressPromise}
 */
export const realm = () => Realm.open({schema: schemas});