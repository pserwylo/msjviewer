import {Body, Button, Left, Right, Content, Header, Text, Title, Subtitle, Spinner} from "native-base";
import {View} from 'react-native';
import {ScreenWrapper} from "./ScreenWrapper";
import {EventIcon} from "./icons/EventIcon";

import React from "react";
import {styles} from './styles';
import * as colours from "./colours";

export const LoadingScreen = (props) =>
    <ScreenWrapper backgroundImage>
        <Header>
            <Left>
                <Button transparent>
                    <EventIcon />
                </Button>
            </Left>
            <Body>
            <Subtitle>(Unofficial)</Subtitle>
            <Title>Mobile MSJ</Title>
            </Body>
            <Right />
        </Header>
        <Content>
            <View style={styles.backgroundOverlay}>
                <Spinner />
                {props.loadingMessage == null ? null :
                    <Text style={{textAlign: "center", color: colours.theme}}>
                        {props.loadingMessage}
                    </Text>}
            </View>
        </Content>
    </ScreenWrapper>;