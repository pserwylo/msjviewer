import React from 'react';

import {STATUS_PENDING_STAFFING, STATUS_ROSTERED, STATUS_SCHEDULED, STATUS_ACTIVE} from "../../events";
import {PendingStaffIcon} from "./PendingStaffIcon";
import {ScheduledIcon} from "./ScheduledIcon";
import {RosteredIcon} from "./RosteredIcon";
import {ActiveIcon} from "./ActiveIcon";

export const StatusIcon = (props) => {
    switch(props.status) {
        case STATUS_PENDING_STAFFING:
            return <PendingStaffIcon colour={props.colour} help={props.help} />;

        case STATUS_SCHEDULED:
            return <ScheduledIcon colour={props.colour} help={props.help} />;

        case STATUS_ROSTERED:
            return <RosteredIcon colour={props.colour} help={props.help} />;

        case STATUS_ACTIVE:
            return <ActiveIcon colour={props.colour} help={props.help} />;

        default:
            throw new Error('Unknown status ' + props.status);
    }
}