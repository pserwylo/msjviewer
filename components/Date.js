import React from 'react';
import {Text} from 'native-base';
import {formatDate, formatTime} from "../events";

export const Date = (props) =>
    <Text style={{color: '#666'}}>
        {formatDate(props.event.startDate)}
    </Text>;