import {List, ListItem, Card, CardItem, Text} from "native-base";
import React from "react";
import {StyleSheet, TouchableOpacity} from "react-native";
import moment from 'moment-timezone';
import {Date} from "./Date";
import {StartEndTime} from "./StartEndTime";
import {View} from 'react-native';
import {STATUS_ROSTERED, STATUS_SCHEDULED, STATUS_NOT_ACCEPTED} from "../events";

const sortEvents = (events) => events.sort(
    (e1, e2) => {
        return moment(e1.startDate).unix() - moment(e2.startDate).unix();
    }
);

export const MyRosterSummaryCard = ({navigation, events}) => {

    const styleForEvent = (event) => {
        switch (event.status) {
            case STATUS_ROSTERED:
                return localStyles.rostered;

            case STATUS_NOT_ACCEPTED:
                return localStyles.rejected;

            default:
                return localStyles.pending;
        }
    };

    const showEvent = (event) => {
        navigation.navigate('Event', {event});
    };

    return (
        <Card>
            <CardItem header bordered>
                <Text>My Upcoming Events</Text>
            </CardItem>
            {sortEvents(events).map(event =>
                <TouchableOpacity key={event.pdn} onPress={() => showEvent(event)}>
                    <CardItem cardBody style={[localStyles.listItem, styleForEvent(event)]}>
                        <View style={{flexDirection: 'column', alignItems: 'flex-start'}}>
                            <Text style={localStyles.eventTitleText}>{event.name}</Text>
                            <RosteredStatus status={event.status} />
                            <View style={localStyles.eventSubtitleText}>
                                <Date event={event}/>
                                <Text> - </Text>
                                <StartEndTime event={event}/>
                            </View>
                        </View>
                    </CardItem>
                </TouchableOpacity>
            )}
        </Card>
    );
};

const RosteredStatus = ({status}) => {
    if (status === STATUS_ROSTERED) {
        return null;
    }

    const text = status === STATUS_NOT_ACCEPTED ? "Not accepted" : "Awaiting approval";
    const style = status === STATUS_NOT_ACCEPTED ? localStyles.rejectedText : localStyles.pendingText;

    return <Text style={style}>{text}</Text>;

};

const localStyles = StyleSheet.create({
    eventTitleText: {
        fontSize: 15,
    },
    eventSubtitleText: {
        fontSize: 13,
        flexDirection: 'row',
    },
    rejectedText: {
        color: 'red',
    },
    pendingText: {
        color: 'orange',
    },
    listItem: {
        paddingLeft: 14,
        paddingRight: 14,
        paddingTop: 8,
        paddingBottom: 8,
        borderStyle: "solid",
        borderBottomWidth: 1,
        borderBottomColor: "#d1d1d1",
    },
    rostered: {
        backgroundColor: "#00ff0022",
    },
    rejected: {
        backgroundColor: "#ff000022",
    },
    pending: {
        backgroundColor: "#ffff0022",
    },
});