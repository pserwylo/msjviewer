import React from 'react';
import {Text} from 'native-base';
import {eventRelativeStartTime} from "../events";

export const RelativeStartTime = ({event}) =>
    <Text style={{color: '#666'}}>
        {eventRelativeStartTime(event)}
    </Text>;