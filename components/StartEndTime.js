import React from 'react';
import {Text} from 'native-base';
import {formatDate, formatTime} from "../events";

export const StartEndTime = (props) =>
    <Text style={{color: '#666'}}>
        {formatTime(props.event.startDate) + " to " + formatTime(props.event.endDate)}
    </Text>;