package com.serwylo.mobilemsj;

import android.os.Bundle;
import android.util.Log;

import com.facebook.react.ReactActivity;
import com.facebook.react.modules.network.OkHttpClientProvider;

public class MainActivity extends ReactActivity {

    /**
     * Returns the name of the main component registered from JavaScript.
     * This is used to schedule rendering of the component.
     */
    @Override
    protected String getMainComponentName() {
        return "MobileMSJ";
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("ReactNative", "Setting up new OkHttpClient");
        OkHttpClientProvider.setOkHttpClientFactory(new MsjHttpClientFactory());
    }
}
