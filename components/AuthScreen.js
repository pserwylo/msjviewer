import React from "react";
import {Card, Left, Title, Body, Right, Button, Content, Form, Header, Input, Item, Label, Text} from "native-base";

import {BackIcon} from "./icons/BackIcon";
import {TickIcon} from "./icons/TickIcon";
import {ScreenWrapper} from "./ScreenWrapper";

export class AuthScreen extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            username: props.username,
            password: props.password,
        };
    }

    render() {
        return (
            <ScreenWrapper backgroundImage>
                <Header>
                    <Left>
                        <Button transparent>
                            <BackIcon onBack={() => this.props.onBack()}/>
                        </Button>
                    </Left>
                    <Body>
                        <Title>Settings</Title>
                    </Body>
                    <Right>
                        <Button transparent onPress={() => this.props.onCredentialsChanged(this.state.username, this.state.password)}>
                            <TickIcon />
                            <Text>Save</Text>
                        </Button>
                    </Right>
                </Header>
                <Content padder>
                    <Card>
                        <Form>
                            <Item floatingLabel>
                                <Label>Username</Label>
                                <Input onChangeText={(text) => this.setState({username: text})} value={this.state.username} />
                            </Item>
                            <Item floatingLabel>
                                <Label>Password</Label>
                                <Input secureTextEntry onChangeText={(text) => this.setState({password: text})} value={this.state.password} />
                            </Item>
                        </Form>
                    </Card>
                </Content>
            </ScreenWrapper>
        );
    }
}