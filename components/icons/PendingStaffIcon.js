import {Icon} from "native-base";

import React from "react";
import {Alert} from "react-native";

const explainIcon = () => Alert.alert("Staff wanted", "Events which are \"Pending Staff\" need more staff to fully cover them. Submit an EOI for these events to help cover them!");

export const PendingStaffIcon = (props) => <Icon style={{color: props.colour}} name='alert' onPress={props.help ? explainIcon : null}/>;