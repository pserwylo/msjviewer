import {Body, Card, CardItem, Text} from "native-base";

import {StyleSheet} from "react-native";
import {STATUS_PENDING_STAFFING, STATUS_SCHEDULED} from "../events";
import React from "react";
import {Grid, Col} from 'react-native-easy-grid';

import {PendingStaffIcon} from "./icons/PendingStaffIcon";
import {ScheduledIcon} from "./icons/ScheduledIcon";
import {EventIcon} from "./icons/EventIcon";
import {events} from "./colours";

export const DivisionSummaryCard = (props) =>
    <Card>
        <CardItem header bordered>
            <Text>{props.division.name}</Text>
        </CardItem>
        <CardItem bordered>
            <Body>
                <Grid>
                    <Col style={{alignItems: 'center'}} onPress={() => props.onViewEventList()}>
                        <EventIcon  />
                        <Text style={localStyles.filterText}>
                            {props.events.length} Event{props.events.length === 1 ? '' : 's'}
                        </Text>
                    </Col>
                    <Col style={[localStyles.filterText, {alignItems: 'center'}]} onPress={() => props.onViewEventList(STATUS_PENDING_STAFFING)}>
                        <PendingStaffIcon colour={events.pending} />
                        <Text style={{color: events.pending, textAlign: "center"}}>
                            {props.events.filter(e => e.status === STATUS_PENDING_STAFFING).length} Pending
                        </Text>
                    </Col>
                    <Col style={[localStyles.filterText, {alignItems: 'center'}]} onPress={() => props.onViewEventList(STATUS_SCHEDULED)}>
                        <ScheduledIcon colour={events.scheduled} />
                        <Text style={{color: events.scheduled, textAlign: "center"}}>
                            {props.events.filter(e => e.status === STATUS_SCHEDULED).length} Scheduled
                        </Text>
                    </Col>
                </Grid>
            </Body>
        </CardItem>
    </Card>;


const localStyles = StyleSheet.create({
    filterText: {
        fontSize: 14,
        textAlign: "center",
    },
});