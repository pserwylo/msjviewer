import {Icon} from "native-base";

import React from "react";
import {Alert} from "react-native";

const explainIcon = () => Alert.alert("Currently running", "This event is \"Active\", meaning it is running right now. You can technically EOI for this event, but you may be unlikely to be rostered.");

export const ActiveIcon = (props) => <Icon style={{color: props.colour}} name='time' onPress={props.help ? explainIcon : null} />;