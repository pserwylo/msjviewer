import {Body, Button, Icon, Left, Right, Content, Header, Text, Title} from "native-base";
import AndroidBackButton from 'react-native-android-back-btn';
import {View} from 'react-native';
import {ScreenWrapper} from "./ScreenWrapper";

import React from "react";
import {styles} from './styles';

export const ErrorScreen = (props) =>
    <ScreenWrapper backgroundImage>
        <Header>
            <Left>
                <AndroidBackButton onPress={() => this.props.onBack()} />
                <Icon name="sad" style={{color: "#fff"}} onPress={() => props.onBack()} />
            </Left>
            <Body>
                <Title>Uh oh...</Title>
            </Body>
            <Right />
        </Header>
        <Content>
            <View style={styles.backgroundOverlay}>
                <Text style={styles.error}>{props.error}</Text>
                <Button style={{marginTop: 16}} onPress={() => props.onBack()}>
                    <Text>Back</Text>
                </Button>
            </View>
        </Content>
    </ScreenWrapper>;