import React from 'react';
import {Text, ScrollView, View} from 'react-native';
import {Button, Icon} from 'native-base';
import {ScreenWrapper} from "../components/ScreenWrapper";
import {EventIcon} from "../components/icons/EventIcon";
import {NoUpcomingEventsCard} from "../components/NoUpcomingEventsCard";
import {MyRosterSummaryCard} from "../components/MyRosterSummaryCard";
import {DivisionSummaryCard} from "../components/DivisionSummaryCard";
import {allEvents} from "../events";

export class MainScreen extends React.Component {

    static navigationOptions = {
        title: 'Mobile MSJ',
        headerLeft: <View style={{marginLeft: 24}}><EventIcon colour='white' /></View>,
        headerRight:
            <View>
                <Button transparent onPress={() => this.props.onSettings()}>
                    <Icon name='settings' />
                </Button>
                <Button transparent onPress={() => this.props.onRefresh()}>
                    <Icon name='refresh' />
                </Button>
            </View>

    };

    onViewEventList(division, status) {
        this.props.navigation.navigate('EventsList', {division, selectedStatus: status});
    }

    render() {
        return (
            <ScreenWrapper backgroundImage>
                <ScrollView>
                    <View style={{padding: 8, flex: 1}}>
                        <View style={{flexGrow: 1}}>
                            {this.props.myUpcomingEvents.length === 0
                                ? <NoUpcomingEventsCard />
                                : <MyRosterSummaryCard navigation={this.props.navigation} events={this.props.myUpcomingEvents}/>}
                        </View>

                        <View style={{flexGrow: 1}}>
                            {this.props.divisions.map(division => (
                                <View key={division.id}>
                                    <DivisionSummaryCard
                                        division={division}
                                        events={this.props.eventsByDivision[division.id]}
                                        onViewEventList={(status = null) => this.onViewEventList(division, status)}
                                    />
                                </View>
                            ))}
                            <View>
                                <DivisionSummaryCard
                                    division={{name: 'All', id: 0}}
                                    events={allEvents(this.props.eventsByDivision)}
                                    onViewEventList={(status = null) => this.onViewEventList(null, status)}
                                />
                            </View>
                        </View>
                    </View>
                </ScrollView>
            </ScreenWrapper>
        )
    }
}