import {Icon} from "native-base";

import React from "react";
import {Alert} from "react-native";

const explainIcon = () => Alert.alert("Full roster", "Events which are \"Scheduled\" already have a full roster. You can EOI for these events, but you will likely only end up on reserve.");

export const ScheduledIcon = (props) => <Icon style={{color: props.colour}} name='people' onPress={props.help ? explainIcon : null} />;