import {StyleSheet} from "react-native";

export const styles = StyleSheet.create({
    backgroundOverlay: {
        backgroundColor: "rgba(255, 255, 255, 0.97)",
        padding: 24,
        margin: 24,
    },
    error: {
        padding: 16,
        backgroundColor: "rgba(255, 160, 160, 0.97)",
        color: "rgba(180, 40, 40, 0.97)",
    },
});