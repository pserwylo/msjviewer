import React from "react";
import moment from 'moment-timezone';

import {Grid, Col, Row} from 'react-native-easy-grid';
import {View} from 'react-native';
import {Card, CardItem, Separator, Picker, List, ListItem, Body, Button, Container, Header, Icon, Left, Right, Title, Text} from "native-base";

import {StatusIcon} from "../components/icons/StatusIcon";
import {eventDuration, STATUS_PENDING_STAFFING, STATUS_ROSTERED, STATUS_SCHEDULED, statusLabel, allEvents} from "../events";
import {StartEndTimeAndDuration} from "../components/StartEndTimeAndDuration";
import {Date} from "../components/Date";
import {RosteredIcon} from "../components/icons/RosteredIcon";
import {ScreenWrapper} from "../components/ScreenWrapper";

export class EventsListScreen extends React.Component {

    static navigationOptions = ({ navigation }) => {
        const divisionName = navigation.getParam('division') ? navigation.getParam('division').name : 'All';
        return {
            title: `${divisionName} Events`,
            headerRight:
                <Button transparent onPress={() => this.props.onRefresh()}>
                    <Icon name='refresh'/>
                </Button>
        }
    };

    constructor(props) {
        super(props);

        const division = props.navigation.getParam('division', null);

        const events = division
            ? props.eventsByDivision[division.id]
            : allEvents(props.eventsByDivision);

        this.state = {
            [STATUS_PENDING_STAFFING]: true,
            [STATUS_SCHEDULED]: true,
            [STATUS_ROSTERED]: true,
            orderBy: 'dateAsc',
            division: division ? division : {name: 'All', id: 0},
            selectedStatus: props.navigation.getParam('selectedStatus'),
            events: events,
        };

        if (this.state.selectedStatus != null) {
            this.state[STATUS_PENDING_STAFFING] = false;
            this.state[STATUS_SCHEDULED] = false;
            this.state[STATUS_ROSTERED] = false;

            this.state[this.state.selectedStatus] = true;
        }
    }

    render() {
        const events = this.sortEvents(this.filteredEvents());

        return (
            <ScreenWrapper>
                <Grid>
                    <Card transparent>
                        <CardItem>
                            <Body>
                                <Row style={{height: 60}}>
                                    {[STATUS_PENDING_STAFFING, STATUS_ROSTERED, STATUS_SCHEDULED].map(status => (
                                        <Col key={status} style={{alignItems: 'center', padding: 2}}>
                                            <StatusToggle
                                                count={this.countEventsByStatus(status)}
                                                status={status}
                                                checked={this.state[status]}
                                                onToggle={() => this.setState({[status]: !this.state[status]})}
                                            />
                                        </Col>
                                    ))}
                                </Row>
                                <Row style={{height: 45}}>
                                    <Picker mode="dropdown" onValueChange={(value) => this.setState({orderBy: value})} selectedValue={this.state.orderBy}>
                                        <Picker.Item label="Sort by: Date (earliest to latest)" value="dateAsc" />
                                        <Picker.Item label="Sort by: Date (latest to earliest)" value="dateDesc" />
                                        <Picker.Item label="Sort by: Duration (shortest to longest)" value="durationAsc" />
                                        <Picker.Item label="Sort by: Duration (longest to shortest)" value="durationDesc" />
                                    </Picker>
                                </Row>
                            </Body>
                        </CardItem>
                    </Card>
                    <Row>
                        {events.length === 0
                            ? <NoEvents />
                            : <List
                                dataArray={events}
                                renderRow={(event) => <EventListItem event={event} onPress={() => this.onShowEvent(event)}/>}
                            />}
                    </Row>
                </Grid>
            </ScreenWrapper>
        );
    }

    onShowEvent(event) {
        this.props.navigation.navigate('Event', { event });
    }

    sortEvents(events) {
        switch(this.state.orderBy) {
            case 'dateAsc': return events.sort(compareDates(false));
            case 'dateDesc': return events.sort(compareDates(true));
            case 'durationAsc': return events.sort(compareDuration(false));
            case 'durationDesc': return events.sort(compareDuration(true));
            default: throw new Error('Cannot sort by ' + this.state.orderBy);
        }
    }

    filteredEvents() {
        return this.state.events.filter(event =>
            event.status === STATUS_PENDING_STAFFING && this.state[STATUS_PENDING_STAFFING] ||
            (event.status === STATUS_ROSTERED || event.rostered) && this.state[STATUS_ROSTERED] ||
            event.status === STATUS_SCHEDULED && this.state[STATUS_SCHEDULED]
        );
    }

    countEventsByStatus(status) {
        return this.state.events.filter(event =>
            event.status === status || event.rostered && status === STATUS_ROSTERED
        ).length;
    }
}


const compareDates = (reverse = false) => (e1, e2, breakTiesWithDuration = true) => {
    const diff = moment(e1.startDate).diff(e2.startDate);
    if (diff !== 0 || !breakTiesWithDuration) {
        return reverse ? -diff : diff;
    }

    return compareDuration(reverse)(e1, e2, false);
};

const compareDuration = (reverse = false) => (e1, e2, breakTiesWithDate = true) => {
    const diff = eventDuration(e1) - eventDuration(e2);
    if (diff !== 0 || !breakTiesWithDate) {
        return reverse ? -diff : diff;
    }

    return compareDates(reverse)(e1, e2, false);
};

const NoEvents = () =>
    <Card transparent>
        <CardItem>
            <Body>
                <Text>
                    No events found.
                </Text>
            </Body>
        </CardItem>
    </Card>;

const EventListItem = ({event, onPress}) =>
    <ListItem onPress={() => onPress()}>
        <Body>
            <View style={{flexDirection: 'column'}}>
                <View>
                    <Text>{event.name}</Text>
                </View>
                <View>
                    <Date event={event} />
                </View>
                <StartEndTimeAndDuration event={event} />
            </View>
        </Body>
        <Right>
            {event.rostered ? <RosteredIcon help /> : <StatusIcon help status={event.status} />}
        </Right>
    </ListItem>;

const StatusToggle = (props) =>
    <Button
        icon
        onPress={() => props.onToggle()}
        bordered={!props.checked}
        style={{height: 55, fontSize: 5}}
        primary={props.checked}>
        <Grid style={{alignItems: 'center'}}>
            <Row>
                <StatusIcon colour={props.checked ? '#fff' : '#bbb'} status={props.status} />
            </Row>
            <Row>
                <Text style={{color: props.checked ? '#fff' : '#bbb'}}>{statusLabel(props.status)} ({props.count})</Text>
            </Row>
        </Grid>
    </Button>;