import React from 'react';
import moment from 'moment-timezone';
import {Text} from 'native-base';
import {eventDuration} from "../events";

export const Duration = (props) =>
    <Text style={{color: '#666'}}>
        {moment.duration(eventDuration(props.event)).humanize()}
    </Text>;