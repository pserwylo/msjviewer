import {Card, CardItem, Text} from "native-base";
import React from "react";

export const NoUpcomingEventsCard = () =>
    <Card>
        <CardItem>
            <Text>You have no upcoming events.</Text>
        </CardItem>
    </Card>;